/* Le CSS placé ici sera appliqué à tous les habillages. */
.rssicon {
display: inline-block;
width: 32px;
height: 32px;
background-image: url(/mw/resources/assets/Feed-icon.png);
}

.hidden {
    display: none;
}

table.infobox {
  width: 30em;
  font-size: 90%;
  border: 1px solid #aaaaaa;
  background-color: #f9f9f9;
  color: black;
  margin-bottom: 0.5em;
  margin-left: 1em;
  padding: 0.2em;
  float: right;
  clear: right;
  text-align:left;
}

table.infobox th.head {
  text-align: center;
  background-color:#ccccff;
}

/* Flexgal */
div.flexgal {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}

div.flexgal div.item {
  display: flexbox;
}
div.flexgal div.item p {
  margin: 0;
  padding: 0; /* padding + margin seem to conflict with the grid layout */
}
a img {
  border: none;
  vertical-align: top; /* magically this cancels the containing anchor's bottom border */
}

div.pubdate {
  clear: both;
}

.Media {
  display: flex;
  align-items: flex-start;
  margin-bottom: 1em;
}

.Media-figure {
  margin-right: 1em;
}

.Media-body {
  flex: 1;
}


/* F O N T E S */
@font-face {
    font-family: "Combined";
    src: url('/typo/Combined/combined.otf');  
    font-style:   normal;
    font-weight:  normal;
}

@font-face {
    font-family: 'combinedregular';
    src: url('/typo/Combined/web/combined.eot');
    src: url('/typo/Combined/web/combined.eot?#iefix') format('embedded-opentype'),
         url('/typo/Combined/web/combined.woff2') format('woff2'),
         url('/typo/Combined/web/combined.woff') format('woff'),
         url('/typo/Combined/web/combined.ttf') format('truetype'),
         url('/typo/Combined/web/combined.svg#combinedregular') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'wremena';
    src: url('typo/Wremema/webfont/wremena_bold-webfont.woff2') format('woff2'),
         url('typo/Wremema/webfont/wremena_bold-webfont.woff') format('woff'),
         url('typo/Wremema/webfont/wremena_bold-webfont.ttf') format('truetype'),
         url('typo/Wremema/webfont/wremena_bold-webfont.svg#wremenabold') format('svg');
    font-weight: bold;
    font-style: normal;
}

@font-face {
    font-family: 'wremena';
    src: url('typo/Wremema/webfont/wremena_regular-webfont.woff2') format('woff2'),
         url('typo/Wremema/webfont/wremena_regular-webfont.woff') format('woff'),
         url('typo/Wremema/webfont/wremena_regular-webfont.ttf') format('truetype'),
         url('typo/Wremema/webfont/wremena_regular-webfont.svg#wremenaregular') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'wremena';
    src: url('typo/Wremema/webfont/wremena_light-webfont.woff2') format('woff2'),
         url('typo/Wremema/webfont/wremena_light-webfont.woff') format('woff'),
         url('typo/Wremema/webfont/wremena_light-webfont.ttf') format('truetype'),
         url('typo/Wremema/webfont/wremena_light-webfont.svg#wremenalight') format('svg');
    font-weight: normal;
    font-style: 200;
}

@font-face {
  font-family: 'Coupeur Monospace Bold';
  src: url('/typo/Coupeur/webfont/CoupeurMonospace-Bold.eot'); /* IE9 Compat Modes */
  src: url('/typo/Coupeur/webfont/CoupeurMonospace-Bold.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('/typo/Coupeur/webfont/CoupeurMonospace-Bold.woff') format('woff'), /* Modern Browsers */
       url('/typo/Coupeur/webfont/CoupeurMonospace-Bold.ttf')  format('truetype'), /* Safari, Android, iOS */
       url('/typo/Coupeur/webfont/CoupeurMonospace-Bold.svg#fcf36b8f65c0f0737cd36a2be6922659') format('svg'); /* Legacy iOS */
       
  font-style:   normal;
  font-weight:  700;
}

@font-face {
  font-family: 'Coupeur Monospace Normal';
  src: url('/typo/Coupeur/webfont/CoupeurMonospace-Normal.eot'); /* IE9 Compat Modes */
  src: url('/typo/Coupeur/webfont/CoupeurMonospace-Normal.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('/typo/Coupeur/webfont/CoupeurMonospace-Normal.woff') format('woff'), /* Modern Browsers */
       url('/typo/Coupeur/webfont/CoupeurMonospace-Normal.ttf')  format('truetype'), /* Safari, Android, iOS */
       url('/typo/Coupeur/webfont/CoupeurMonospace-Normal.svg#4b219f539d302a6ffd9fd41e2da16172') format('svg'); /* Legacy iOS */
       
  font-style:   normal;
  font-weight:  400;
}


/* NEW RULES (MICHAEL ) */

body, body p {
    font-family: "Coupeur Monospace Normal", monospace;
}
h1, h2, h3, h4 {
        font-family: 'combinedregular','Combined', monospace !important;
        padding-right: 10%;
        padding-top: 0.5em;
        margin: 0;
        border-color: black;
        line-height: 1.5em;
        font-weight: normal;
font-size: 100%;
}
div.header-container {
  /* display: none; */
}
form.header {
  display: none;
}
ul#page-actions {
  display: none;
}
a.talk {
  display:none;
}
