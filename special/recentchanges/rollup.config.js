// rollup.config.js
// https://github.com/rollup/rollup-plugin-commonjs
import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';

export default [{
  input: 'src/recentchanges.js',
  output: {
    file: 'dist/recentchanges.js',
    format: 'iife',
    name: 'app'
  },
  plugins: [
    resolve(), 
    commonjs()
  ]
}];