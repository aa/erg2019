
var svg = document.querySelector("#svg"),
    iframe = document.querySelector("iframe#wikiframe"),
    map = new app.Map({
        apiurl: "/mw/api.php",
        symbols: "src/legend.json",
        svg: "#svg",
        categorylabel: "Catégorie",
        categorydiv: "#cats .body",
        zoom: 2.0
    }),
    zoom_in = document.querySelector(".leaflet-control-zoom-in"),
    zoom_out = document.querySelector(".leaflet-control-zoom-out"),
    cats = document.querySelector("#cats");

window.addEventListener("resize", resize);
function resize() {
    var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;
    // svg.setAttribute("width", x);
    // svg.setAttribute("height", y);
    //console.log("resize", x, y);

    // Match size of parent
    svg.setAttribute("width", page.clientWidth);
    svg.setAttribute("height", page.clientHeight);

    // Checks if narrow device
    if (x>600){ g.querySelector("#cats").classList.add("expanded")}
}

async function doload () {
    resize();
    console.log("map.init");
    await map.init();
    console.log("map.init: done");

    map.on("page", function (page) {
        // console.log("map.page", page.title);
        var url = page.url();
        if (iframe.src !== url) {
            // console.log("setting iframe src to", url);
            iframe.src = url;
        }
    })
    zoom_in.addEventListener("click", (e) => {
        map.zoom_in();
    })
    zoom_out.addEventListener("click", (e) => {
        map.zoom_out();
    })
}
window.addEventListener("DOMContentLoaded", doload);
iframe.addEventListener("load", function () {
    map.set_active_url(iframe.contentWindow.location.href);
});

cats.addEventListener("click", e => {
    cats.classList.toggle("expanded");
});

var specialselect = document.querySelector("select#special"),
    // specialselectdiv = document.getElementById("specialselectdiv"),
    specialiframediv = document.getElementById("specialiframediv"),
    specialiframe = null,
    specialsrc = null,
    specialclosediv = document.getElementById("specialclosediv"),
    specialclosebutton = document.getElementById("specialclosebutton");

function set_special (v) {
    if (specialsrc !== v) {
        specialsrc = v;
        specialselect.value = v;
        if (specialsrc !== "") {
            if (specialiframe == null) {
        //  <iframe id="specialframe" name="specialframe" src="/m/special/recentfiles/recentfiles.html"></iframe>
                specialiframe = document.createElement('iframe');
                specialiframediv.appendChild(specialiframe);
            }
            // console.log("specialiframe.src", specialiframe.src)
            if (specialiframe.getAttribute("src") != specialsrc) {
                console.log("setAttribute iframe src", specialsrc);
                specialiframe.setAttribute("src", specialsrc);
            }
            specialiframediv.style.display = "block";
            specialclosediv.style.display = "block";
        } else {
            // show map
            // if (specialiframe) {
            //     specialiframe.src = "";
            //     specialiframediv.removeChild(specialiframe);
            //     specialiframe = null;
            // }
            specialiframediv.style.display = "none";
            specialclosediv.style.display = "none";
        }
    }
}
set_special(specialselect.value);
specialclosebutton.addEventListener("click", function (e) {
    e.preventDefault();
    set_special("");
});
specialselect.addEventListener("input", function (e) {
    console.log("specialselect", specialselect.value);
    set_special(specialselect.value);
});
